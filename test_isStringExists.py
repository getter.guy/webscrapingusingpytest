import pytest
import re

@pytest.mark.parametrize("str",[(".*Right-click in the box below to see one called \'the-internet\'.*"),(".*Alibaba.*")])
def test_str(supply_scraper,str):
    soup = supply_scraper[0]
    res = supply_scraper[1]
    for inp in soup.find_all('div'):
        if re.search(str, inp.get_text()):
            print("found")
            res = 1
    assert res == 1, "str "+ str + " (regexp format) not found in the given html"
