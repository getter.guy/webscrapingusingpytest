import pytest
import requests
from bs4 import BeautifulSoup

@pytest.fixture
def supply_scraper():
    r=requests.get("https://the-internet.herokuapp.com/context_menu")
    content=r.content
    soup = BeautifulSoup(content,"html.parser")
    res = 0
    return [soup,res]
